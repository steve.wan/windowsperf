// BSD 3-Clause License
//
// Copyright (c) 2022, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#pragma once

#include <stdio.h>

//
// Declare some Kernel-Mode debug macros helpers
//
#define WindowsPerfKdPrint(format, ...)			DbgPrint("WindowsPerf: "format, __VA_ARGS__);
#define WindowsPerfKdPrintInfo(format, ...)     DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "WindowsPerf: "format, __VA_ARGS__);

inline void WindowsPerfKdPrintBuffer(unsigned char *pBuffer, unsigned long inputSize)
{
    if (!pBuffer)
        return;

    char str[128] = { 0 };
    const unsigned long str_size = sizeof(str) / sizeof(str[0]);
    const int elems_in_line = 8;

    unsigned long i, index = 0;
    for (i = 0; i < inputSize; i++)
    {
        unsigned char* p = (unsigned char*)pBuffer;
        if (i && !(i % elems_in_line))
        {
            WindowsPerfKdPrint("pBuffer[%4d]=%s\n", i, str);
            index = 0;
            str[0] = '\0';
        }
        index += sprintf_s(str + index, str_size - index, "%02x ", p[i]);
    }
    WindowsPerfKdPrint("pBuffer[%4d]=%s\n", i, str);
}